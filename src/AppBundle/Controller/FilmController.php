<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Film;
use AppBundle\Form\FilmType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class FilmController extends Controller
{
    /**
     * @Route("/", name="films")
     */
    public function indexAction()
    {
        $entityManager = $this->getDoctrine()->getManager();

        /** @var  $films array|null Film collection */
        $films = $entityManager->getRepository('AppBundle:Film')->findBy(
            ['isActive' => true]
        );
        return $this->render(
            'AppBundle::index.html.twig',
             array('films' => $films)
        );
    }

    /**
     * @Route("/film/new", name="new_film")
     */
    public function newAction()
    {
        /** @var $film Film */
        $film = new Film();
        /** @var $form Form*/
        $form = $this->createNewForm($film);

        return $this->render(
            'AppBundle::new.html.twig',
            array('form' => $form->createView())
        );
    }

    /**
     * @Route("/film/create", name="create_film")
     */
    public function createAction(Request $request)
    {
        /** @var $film Film */
        $film = new Film();
        /** @var $form Form*/
        $form = $this->createNewForm($film);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($film);
            $em->flush();
            return $this->redirect($this->generateUrl('films_dashboard'));
        }
        return $this->render(
            'AppBundle::new.html.twig',
            array('form' => $form->createView())
        );
    }

    /**
     * Creates a form to create a Film entity.
     *
     * @param Film $film The entity
     *
     * @return Form The form
     */
    private function createNewForm(Film $film)
    {
        $form = $this->createForm(new FilmType(), $film, array(
            'action' => $this->generateUrl('create_film'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create', 'attr' => array('class' => 'btn btn-success')));

        return $form;
    }

    /**
     * @Route("/dashboard/films", name="films_dashboard")
     */
    public function dashboardFilmsAction()
    {
        $entityManager = $this->getDoctrine()->getManager();

        /** @var  $films array|null Film collection */
        $films = $entityManager->getRepository('AppBundle:Film')->findAll();
        return $this->render(
            'AppBundle::dashboard.html.twig',
            array('films' => $films)
        );
    }

    /**
     * @Route("/film/edit/{id}", name="edit_film")
     */
    public function editAction($id)
    {
        /** @var $film Film */
        $film = $this->getDoctrine()->getManager()->getRepository('AppBundle:Film')->findOneById($id);
        if (!$film) {
            throw new NotFoundHttpException("Старница не найдена");
        }
        /** @var $form Form*/
        $form = $this->createEditForm($film);

        return $this->render(
            'AppBundle::new.html.twig',
            array('form' => $form->createView())
        );
    }

    /**
     * @Route("/film/update/{id}", name="update_film")
     */
    public function updateAction(Request $request, $id)
    {
        /** @var $film Film */
        $film = $this->getDoctrine()->getManager()->getRepository('AppBundle:Film')->findOneById($id);
        if (!$film) {
            throw new NotFoundHttpException("Старница не найдена");
        }

        /** @var $form Form*/
        $form = $this->createEditForm($film);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();
            return $this->redirect($this->generateUrl('films_dashboard'));
        }
        return $this->render(
            'AppBundle::new.html.twig',
            array('form' => $form->createView())
        );
    }

    /**
     * Creates a form to create a Film entity.
     *
     * @param Film $film The entity
     *
     * @return Form The form
     */
    private function createEditForm(Film $film)
    {
        $form = $this->createForm(new FilmType(), $film, array(
            'action' => $this->generateUrl('update_film', array('id' => $film->getId())),
            'method' => 'POST',
        ));
        $form->add('submit', 'submit', array('label' => 'Update', 'attr' => array('class' => 'btn btn-info')));
        return $form;
    }

    /**
     * @Route("/film/delete/{id}", name="delete_film")
     */
    public function deleteAction($id)
    {
        /** @var $film Film */
        $film = $this->getDoctrine()->getManager()->getRepository('AppBundle:Film')->findOneById($id);
        if (!$film) {
            throw new NotFoundHttpException("Старница не найдена");
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($film);
        $em->flush();
        return $this->redirect($this->generateUrl('films_dashboard'));

    }
}
