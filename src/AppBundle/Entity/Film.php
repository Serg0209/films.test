<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 *
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FilmRepository")
 * @ORM\Table(name="films")
 */
class Film
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank(message="Поле не должно быть пустым")
     * @Assert\Length(
     *      min = 3,
     *      max = 256,
     *      minMessage = "Название должно быть длинее 3 символов",
     *      maxMessage = "Название должно быть не более 256 символов"
     * )
     */
    protected $name;
    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank(message="Поле не должно быть пустым")
     * @Assert\Range(
     *      min = 1900,
     *      max = 2020,
     *      minMessage = "Год выпуска фильма должен быть не ранее 1900 года",
     *      maxMessage = "Год выпуска фильма должен быть не позже 2020 года"
     * )
     */
    protected $year;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $isActive = true;

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Film
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @param boolean $isActive
     * @return Film
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
        return $this;
    }

    /**
     * @return integer
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param integer $year
     * @return Film
     */
    public function setYear($year)
    {
        $this->year = $year;
        return $this;
    }
}